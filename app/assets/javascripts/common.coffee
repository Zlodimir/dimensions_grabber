$ ->
  $('.js-submit-form').on 'click', (e) ->
    e.preventDefault()
    $(@).closest('form').submit()

  $('body').on 'ajax:before', '.search-form', ->
    $('#waiting-layout').fadeIn()

  $('body').on 'ajax:complete', '.search-form', ->
    $('#waiting-layout').fadeOut()

  $('body').on 'ajax:success', '.search-form', (e, data, status, xhr) ->
    if data
      $('.result-block').empty().append(data)
