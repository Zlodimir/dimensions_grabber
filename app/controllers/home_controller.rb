class HomeController < ApplicationController
  def index
  end

  def search
    @results = Search::Service.new.find({ text: params[:text] })

    render layout: false
  end
end
