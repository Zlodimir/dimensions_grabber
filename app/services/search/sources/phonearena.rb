module Search
  module Sources
    class Phonearena
      require 'nokogiri'

      ACTION_URL = "http://www.phonearena.com"

      def self.get_data(params = { })
        page = Nokogiri::HTML(open("#{ACTION_URL}/search?term=#{params[:text]}"))
        return nil if page.css('.s_block_4').count == 0
        # let's find first item in the search result
        link = page.css('.s_block_4 a')[0]["href"]
        # let's find dimensions and others on the phone's page
        page = Nokogiri::HTML(open("#{ACTION_URL}#{link}"))
        dimensions = page.css('span.s_size_rating')[0].parent.text()
        model = page.css('h1')[0].css('span')[0].text()
        image = 'http:' + page.css('img')[3].attr('src')
        
        {
          dimensions: dimensions,
          model: model,
          image: image
        }
      end
    end
  end
end
