module Search
  class Service
    attr_reader :text

    # array of sources
    SOURCES = ["phonearena"]

    def find(params = { })
      @text = params[:text]

      results = []
      SOURCES.each do |source|
        source.capitalize!
        results << "Search::Sources::#{source}".constantize.get_data(({ text: text }))
      end

      results.reject { | el | el.nil? }
    end
  end
end
